#!/usr/bin/env python3


import argparse
import browser
import logging
import os.path
import re


logging.getLogger().setLevel(logging.DEBUG)
browser = browser.Browser()


parser = argparse.ArgumentParser()
parser.add_argument("input")
arguments = parser.parse_args()


base = "http://ruexport.org"
with open(arguments.input, "r") as input:
    urls = [base + path.strip() for path in input.readlines()]


output = "html/%d.html"
digits = re.compile("\d+")
for url in urls:
    id = digits.search(url).group()
    output_ = output % int(id)

    if os.path.isfile(output_):
        logging.info("File %s already exits, skipping the page." % output_)
        continue

    browser.download(url, output_)
