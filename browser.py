import logging
import subprocess


# Before you ask: why the hell do you need to enroll your own browser?
# Well, because sometimes developers want to protect their data from
# scrapers like us by evaluating some javascript on client-side.
# That's why I'm using PhantomJS (which, actually, is just a headless
# WebKit browser) with a page saving script and wrapping it into this
# simple python wrapper. Why don't I just script all of this via bash
# you ask? Well, I don't know any bash :)
class Browser():

    command = "phantomjs savepage.js %s"

    def fetch(self, url):
        logging.info("Fetching page %s" % url)
        command = Browser.command % url
        arguments = command.split()
        process = subprocess.Popen(arguments, stdout=subprocess.PIPE)
        content = process.stdout.read()
        logging.info("Finished fetching")
        return content

    def download(self, url, filename):
        content = self.fetch(url)
        if len(content) == 0:
            logging.error("Could not fetch %s" % url)
            return
        logging.info("Storing %s into %s" % (url, filename))
        with open(filename, "wb") as output:
            output.write(content)
