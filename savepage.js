// Phantom JS script that save the web-page passed as first argument
// in the command-line.

var system = require("system");
var webpage = require("webpage").create();

url = system.args[1]
webpage.open(url, function (status) {
    if (status == "success") {
        console.log(webpage.content);
    }
    phantom.exit();
});
