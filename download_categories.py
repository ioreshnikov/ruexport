#!/usr/bin/env python3


import browser
import logging


logging.getLogger().setLevel(logging.DEBUG)
browser = browser.Browser()


# Magic number 100500 in path is actually the number of items to
# display per page. Since developers didn't bother to validate this,
# we can pretty much scrape all the category in single call.
base = "http://ruexport.org/rus_export_catalog/"
path = "?action=areas&s=0&id=%d&rows=100500"
url = base + path
output = "category/%d.html"


categories = range(4, 29)  # Don't ask me why!
for category in categories:
    url_ = url % category
    output_ = output % category
    browser.download(url_, output_)
