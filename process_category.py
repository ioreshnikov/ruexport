#!/usr/bin/env python


import argparse
import lxml.html


parser = argparse.ArgumentParser()
parser.add_argument("input", type=str)
arguments = parser.parse_args()


with open(arguments.input) as input:
    html = lxml.html.parse(input)


paths = html.xpath('//a[@class="company-link"]/@href')
for path in paths:
    print(path)
