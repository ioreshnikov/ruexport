#!/usr/bin/env python3


import argparse
import json
import lxml.html
import re


parser = argparse.ArgumentParser()
parser.add_argument("input", type=str)
arguments = parser.parse_args()


with open(arguments.input, "r") as input:
    html = lxml.html.parse(input)


company = {}
company["name"] = html.xpath("//h1/text()")[0]


contactlist = "//div[@class='contact-block']/table/tbody/tr/td/ul/li"

address = html.xpath(contactlist + "[1]/text()")[0].strip()
address = " ".join(address.split())
company["address"] = address

phones = html.xpath(contactlist + "[2]/text()")[0]
phones = phones.replace("Телефон компании: ", "").split(", ")
company["phones"] = phones

email = html.xpath(contactlist + "[3]/span/a/@href")[0]
email = email.replace("mailto:", "")
company["email"] = email

website = html.xpath(contactlist + "[4]/a/@href")[0]
company["website"] = website


description = "//td[@colspan='2']/"
company["description"] = html.xpath(description + "p/text()")[0].strip()

keywords = html.xpath(description + "i/text()[last()]")[0].strip()
keywords = keywords.replace(": ", "")
company["keywords"] = keywords.split("\n")


company["contact"] = {}
contact = "//div[@class='one-contact-side']/div[@class='info']"

name = html.xpath(contact + "/div[1]/b/text()")
if len(name) != 0:
    name = name[0].strip()
    company["contact"]["name"] = name

    position = html.xpath(contact + "/div[2]/text()")[0]
    company["contact"]["position"] = position

    phones = html.xpath(contact + "/div[3]/text()")[0].strip()
    phones = phones.split(", ")
    company["contact"]["phones"] = phones

    email = html.xpath(contact + "/span/button/@onclick")[0].strip()
    email = re.search("mailto:(.*)'", email).group(1)
    company["contact"]["email"] = email


company["business"] = {}
business = "//div[@class='props-side']/ul/"

age = html.xpath(
    "//li/label[text()='Возраст компании']/following-sibling::*/text()")
if len(age) != 0:
    company["business"]["age"] = age[0]

size = html.xpath(
    "//li/label[text()='Количество сотрудников']/following-sibling::*/text()")
if len(size) != 0:
    company["business"]["size"] = size[0]

size = html.xpath(
    "//li/label[text()='Кол-во сотрудников']/following-sibling::*/text()")
if len(size) != 0:
    company["business"]["size"] = size[0]

turnover = html.xpath(
    "//li/label[text()='Годовой оборот (млн USD)']" +
    "/following-sibling::*/text()")
if len(turnover) != 0:
    company["business"]["turnover"] = turnover[0]

type = html.xpath(
    "//li/label[text()='Тип бизнеса']/following-sibling::*/text()")
if len(type) != 0:
    company["business"]["type"] = type[0]

languages = html.xpath(
    "//li/label[text()='Языки общения']/following-sibling::*/text()")
if len(languages) != 0:
    company["business"]["languages"] = languages[0].strip().split(", ")


company["presence"] = {}
presence = (
    "//li/label[text()='Присутствие на рынках']/following-sibling::div/ul/")

regions = html.xpath(presence + "li/text()[1]")
regions = [region.strip() for region in regions]

countrises = html.xpath(presence + "/div[@class='wrap-more-region']/text()")
countrises = [countries.strip().split(", ") for countries in countrises]
company["presence"] = dict(zip(regions, countrises))


company["wishlist"] = {}
wishlist = (
    "//li/label[text()='Желаемые рынки']/following-sibling::div/ul/")

regions = html.xpath(wishlist + "li/span/text()")
regions = [region.strip() for region in regions]

countrises = html.xpath(
    wishlist + "li/div/div[@class='wrap-more-region']/text()")
countrises = [countries.strip().split(", ") for countries in countrises]
company["wishlist"] = dict(zip(regions, countrises))


record = json.dumps(company,
                    indent=4,
                    sort_keys=True,
                    ensure_ascii=False)
print(record)
