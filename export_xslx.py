#!/usr/bin/env python3


import argparse
import json
import logging
import os
import os.path
import string
import xlsxwriter


logging.getLogger().setLevel(logging.DEBUG)


parser = argparse.ArgumentParser()
parser.add_argument("directory")
arguments = parser.parse_args()


table = []
columns = []
for filename in os.listdir(arguments.directory):
    filename = os.path.join(arguments.directory, filename)

    with open(filename, "r") as fd:
        try:
            logging.info("Reading %s" % filename)
            profile = json.load(fd)
        except:
            logging.error("Malformed profile for %s" % filename)

    row = []

    columns.append({"header": "Название"})
    name = profile.get("name")
    row.append(name)

    columns.append({"header": "Адрес"})
    address = profile.get("address")
    row.append(address)

    columns.append({"header": "Почта"})
    email = profile.get("email")
    row.append(email)

    columns.append({"header": "Телефоны"})
    phones = profile.get("phones")
    if phones is not None:
        phones = ", ".join(phones)
    row.append(phones)

    columns.append({"header": "Веб-сайт"})
    website = profile.get("website")
    row.append(website)

    business = profile.get("business", {})

    columns.append({"header": "Тип"})
    type = business.get("type")
    row.append(type)

    columns.append({"header": "Возраст"})
    age = business.get("age")
    row.append(age)

    columns.append({"header": "Размер"})
    size = business.get("size")
    row.append(size)

    columns.append({"header": "Годовой оборот, млн. USD"})
    turnover = business.get("turnover")
    row.append(turnover)

    contact = profile.get("contact", {})

    columns.append({"header": "Контактное лицо"})
    name = contact.get("name")
    row.append(name)

    columns.append({"header": "Должность"})
    position = contact.get("position")
    row.append(position)

    columns.append({"header": "Почта"})
    email = contact.get("email")
    row.append(email)

    columns.append({"header": "Телефоны"})
    phones = contact.get("phones")
    if phones is not None:
        phones = ", ".join(phones)
    row.append(phones)

    table.append(row)


startcell = "a1".upper()
endcell = string.ascii_uppercase[len(row)] + str(len(table) + 1)

workbook = xlsxwriter.Workbook("ruexport.xlsx")
worksheet = workbook.add_worksheet()
worksheet.add_table("%s:%s" % (startcell, endcell),
                    {"columns": columns, "data": table})
workbook.close()
